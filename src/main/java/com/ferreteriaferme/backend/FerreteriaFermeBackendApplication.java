package com.ferreteriaferme.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class FerreteriaFermeBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(FerreteriaFermeBackendApplication.class, args);
	}

}
