package com.ferreteriaferme.backend.service;

import java.util.List;

import com.ferreteriaferme.backend.entity.Producto;

public interface IProductoService {

	public List<Producto> findAll();
}
