package com.ferreteriaferme.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ferreteriaferme.backend.entity.Producto;
import com.ferreteriaferme.backend.service.ProductoServiceImpl;


@CrossOrigin(origins = {"*"})
@RestController
@RequestMapping("/api")
public class ProductoController {

	@Autowired
	private ProductoServiceImpl productoService;
	
	@GetMapping("/productos")
	public List<Producto> obtenerProductos(){
		
		return productoService.findAll();
	}
}
