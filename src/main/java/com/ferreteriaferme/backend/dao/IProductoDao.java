package com.ferreteriaferme.backend.dao;

import org.springframework.data.repository.CrudRepository;

import com.ferreteriaferme.backend.entity.Producto;

public interface IProductoDao extends CrudRepository<Producto, Long>{

}
